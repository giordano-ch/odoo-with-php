<?php
    require 'odoo/OdooEventReader.php';
    $events = new OdooEventReader();
    $records = $events->GetEvents();

    $catgoryList = $events->GetCategoryList();
?>

    <div id="c1645" class="csc-default">
        <div class="rightcolBox">
            <div id="c1013" class="csc-default">
                <div class="csc-header csc-header-n1">
                    <h2 class="csc-firstHeader">Agenda</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="rightcolBox">
        <?php
            foreach($records as $key=>$value) {
                if($value['gio_favcourse'] === true) {
        ?>
                    <p>
                        <span class="downloadIcalLink glyphicon glyphicon-calendar">
                            <a href="<?php echo 'http://oekowatt01.nine.ch:8069'.$value['website_url']; ?>" target="_blank" title="<?php echo utf8_encode($value['name']); ?>" target="_blank" class="download">
                                <?php echo utf8_encode($value['name']); ?>
                            </a>
                        </span>
                    </p>

        <?php
                }
            }
        ?>
    </div>
