<?php
// include bibliothek für die abfrage von XMLRPC --> Die Bibliothek welche auf der Odoo Seite als Beispiel kommt.
require 'ripcord/ripcord.php';

// Login Einstellungen
$url = 'http://oekowatt01.nine.ch:8069';
$db = 'z_oekowatt_init_Test_Events_Typo_4-2017-03-07';
$username = 'admin';
$password = '6343OekoWatt';
$model = 'event.type';

$common = ripcord::client("$url/xmlrpc/2/common");
$common->version();

$uid = $common->authenticate($db, $username, $password);

$models = ripcord::client("$url/xmlrpc/2/object");

$records = $models->execute_kw($db, $uid, $password, $model, 'search', array());


foreach($records as $record)
{
    try{
        echo $record;
    }
    catch (Exception $ex){ }
}

?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Odoo Events</title>

    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">

    <!--
    <div class="row">
        <div class="col-md-12">
            <h1>Odoo Events</h1>
        </div>
    </div>
    -->

    <div class="row">

        <?php foreach($records as $value) { ?>

            <div class="col-md-4">

                <p>
                    <?php echo $value['id']; ?>
                </p>
                <p>
                    <?php echo $value['name']; ?>
                </p>

            </div>

        <?php } ?>

    </div>

</div>

<script src="../node_modules/jquery.2/node_modules/jquery/dist/jquery.min.js"></script>
<script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>