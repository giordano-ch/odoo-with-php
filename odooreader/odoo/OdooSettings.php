<?php

require __DIR__ . "/../ripcord/ripcord.php";

class OdooSettings
{
    // Odoo URL
    static public $url = 'http://oekowatt01.nine.ch:8069';
    // Odoo Datenbank
    static public $db = 'z_oekowatt_init_Test_Events_Typo_4-2017-03-07';
    // Odoo Benutzer (mit den entsprechenden Berechtigungen!)
    static public $username = 'admin';
    // Odoo Passwort
    static public $password = '6343OekoWatt';
    // Odoo Sprache
    static public $language = 'de_CH';
    // Odoo Sprach ID
    static public $languageId = 2;

    // Authentication für Odoo
    // wird bei jedem Aufruf benötigt
    public function Authenticate()
    {
        $uid = 0;

        try {
            $common = ripcord::client(self::$url."/xmlrpc/2/common");
            $common->version();

            $uid = $common->authenticate(self::$db, self::$username, self::$password, array());
        } catch (Exception $e) {
            echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
        }

        return $uid;
    }

    // Abfrage res.partner
    public function GetResPartnerById($address_id)
    {
        $uid = self::Authenticate();
        $models = ripcord::client(self::$url."/xmlrpc/2/object");

        $records = $models->execute_kw(self::$db, $uid, self::$password,
            'res.partner', 'read',
            array($address_id),
            array(
                'fields'=>array('id', 'name', 'street', 'zip', 'city', 'country_id', 'email'),
                'context' => array('lang' => 'de_CH')
            ));

        return $records;
    }

    // Abfrage res.country
    public function GetResCountryById($country_id)
    {
        $uid = self::Authenticate();
        $models = ripcord::client(self::$url."/xmlrpc/2/object");

        $records = $models->execute_kw(self::$db, $uid, self::$password,
            'res.country', 'read',
            array($country_id),
            array(
                'fields'=>array('id', 'name'),
                'context' => array('lang' => 'de_CH')
            ));

        return $records;
    }
}