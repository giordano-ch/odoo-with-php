<?php

require 'OdooSettings.php';

class OdooEventReader extends OdooSettings
{
    // Abfrage aller veröffentlichter Events
    public function GetEvents()
    {
        // Aktuelles Datum auslesen
        date_default_timezone_set('UTC');
        $currentDate = date("c");

        // Zugriff in Odoo aufrufen und die Aktuelle ID zurück bekommen
        $uid = parent::Authenticate();

        // RipCord RPC Client initialisieren
        $models = ripcord::client(parent::$url."/xmlrpc/2/object");

        // Suchen nach allen veröffentlichten Events
        $ids = $models->execute_kw(parent::$db, $uid, parent::$password,
            'event.event', 'search', array(
                array(
                    array('website_published', '=', "1"),
                    array('date_begin', '>=', $currentDate)
                )));

        // Lesen aller gefunden Events anhand der IDs
        $records = $models->execute_kw(parent::$db, $uid, parent::$password,
            'event.event', 'read',
            array($ids),
            array(
                'fields' => array('id', 'name', 'date_begin', 'date_end', 'address_id', 'event_type_id', 'description', 'gio_favcourse', 'gio_opencourse', 'gio_running_workshops', 'event_ticket_ids', 'display_name', 'website_url', 'organizer_id'),
                'context' => array('lang' => 'de_CH')
            ));

        // Zurück geben aller gefunden Resultate
        return $records;
    }

    // Liest anhand des Events die Ticketpreise aus
    public function GetEventTickets($eventTicketIds)
    {
        $uid = parent::Authenticate();
        $models = ripcord::client(parent::$url."/xmlrpc/2/object");

        $records = $models->execute_kw(parent::$db, $uid, parent::$password,
            'event.event.ticket', 'read',
            array($eventTicketIds),
            array(
                'fields'=>array('id', 'name', 'price'),
                'context' => array('lang' => 'de_CH')
            ));

        return $records;
    }

    public function GetCategoryList()
    {
        $uid = parent::Authenticate();
        $models = ripcord::client(parent::$url."/xmlrpc/2/object");

        $records = $models->execute_kw(parent::$db, $uid, parent::$password,
            'event.type', 'search', []);

        return $records;
    }

    // Liest den Event Type anhand der ID und gibt den Wert zurück
    public function GetEventType($eventTypeId)
    {
        $uid = parent::Authenticate();
        $models = ripcord::client(parent::$url."/xmlrpc/2/object");

        $records = $models->execute_kw(parent::$db, $uid, parent::$password,
            'event.type', 'read',
            array($eventTypeId),
            array(
                'fields'=>array('id', 'name'),
                'context' => array('lang' => 'de_CH')
            ));

        return $records;
    }
}