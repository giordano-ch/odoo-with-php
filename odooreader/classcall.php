<?php
    require 'odoo/OdooEventReader.php';
    $events = new OdooEventReader();
    $records = $events->GetEvents();

    $catgoryList = $events->GetCategoryList();
?>

<?php
$monatsnamen = array(
    1=>"Jan",
    2=>"Feb",
    3=>"Mär",
    4=>"Apr",
    5=>"Mai",
    6=>"Jun",
    7=>"Jul",
    8=>"Aug",
    9=>"Sep",
    10=>"Okt",
    11=>"Nov",
    12=>"Dez");

$monatsnamenLang = array(
    1=>"Januar",
    2=>"Februar",
    3=>"März",
    4=>"April",
    5=>"Mai",
    6=>"Juni",
    7=>"Juli",
    8=>"August",
    9=>"September",
    10=>"Oktober",
    11=>"November",
    12=>"Dezember");

$weekdayName = array(
    0=>"Mo",
    1=>"Di",
    2=>"Mi",
    3=>"Do",
    4=>"Fr",
    5=>"Sa",
    6=>"So",
);
?>

            <div id="c1600" class="csc-default">
                <div class="csc-header csc-header-n1">
                    <h1 class="csc-firstHeader">Agenda</h1>
                </div>

                <div class="tx-nezzoagenda">

                    <!--
                    <input type="hidden" id="agenda_pageUid" autocomplete="off" value="351">
                    <input type="hidden" id="agenda_contentElementId" autocomplete="off" value="1600">

                    <input type="hidden" id="agenda_countOfEntriesStartup" autocomplete="off" value="10">
                    <input type="hidden" id="agenda_countOfAllEvents" autocomplete="off" value="45">

                    <input type="hidden" id="agenda_archiveMode" autocomplete="off" value="0">

                    <input type="hidden" id="agenda_currentDisplayedEvents" autocomplete="off" value="10">
                    <input type="hidden" id="agenda_currentSortField" autocomplete="off" value="start_of_event">
                    <input type="hidden" id="agenda_currentSortDirection" autocomplete="off" value="asc">

                    <input type="hidden" id="typeNumPage" autocomplete="off" value="0">
                    <input type="hidden" id="currentLanguage" autocomplete="off" value="0">

                    <input type="hidden" id="detailsPid" autocomplete="off" value="203">

                    <div id="searchBar">
                        <div id="searchField" class="searchBarItem">
                            <input name="searchField" id="agenda_searchField" placeholder="Suchen">
                        </div>

                        <div id="searchCategories" class="searchBarItem">
                            <select id="agenda_categories" name="category">
                                <option>Kategorie wählen...</option>
                                <?php
                                echo $catgoryList;
                                    foreach($catgoryList as $category) {
                                        echo $category['id'];
                                ?>

                                <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>

                                <?php } ?>
                            </select>
                            <span class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" tabindex="0" id="agenda_categories-button" role="combobox" aria-expanded="false" aria-autocomplete="list" aria-owns="agenda_categories-menu" aria-haspopup="true" aria-activedescendant="ui-id-1" aria-labelledby="ui-id-1" aria-disabled="false" style="width: 100%;"><span class="ui-icon ui-icon-triangle-1-s"></span><span class="ui-selectmenu-text">Kategorie wählen ...</span></span>
                        </div>

                        <div id="searchAdditionalCategories" class="searchBarItem">
                            <select id="agenda_additionalCategories" name="additionalCategory" style="display: none;"><option value="0" selected="selected">Subkategorie wählen ...</option>
                            </select><span class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" tabindex="0" id="agenda_additionalCategories-button" role="combobox" aria-expanded="false" aria-autocomplete="list" aria-owns="agenda_additionalCategories-menu" aria-haspopup="true" aria-activedescendant="ui-id-21" aria-labelledby="ui-id-21" aria-disabled="false" style="width: 100%;"><span class="ui-icon ui-icon-triangle-1-s"></span><span class="ui-selectmenu-text">Subkategorie wählen ...</span></span>
                        </div>

                        <div id="searchMonths" class="searchBarItem">
                            <select id="agenda_months" name="month" style="display: none;"><option value="0" selected="selected">Monat wählen...</option>
                                <option value="2_2017">Februar 2017</option>
                                <option value="3_2017">März 2017</option>
                                <option value="4_2017">April 2017</option>
                                <option value="5_2017">Mai 2017</option>
                                <option value="6_2017">Juni 2017</option>
                                <option value="8_2017">August 2017</option>
                                <option value="9_2017">September 2017</option>
                                <option value="10_2017">Oktober 2017</option>
                                <option value="11_2017">November 2017</option>
                                <option value="13">Laufende Workshops</option>
                            </select><span class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" tabindex="0" id="agenda_months-button" role="combobox" aria-expanded="false" aria-autocomplete="list" aria-owns="agenda_months-menu" aria-haspopup="true" aria-activedescendant="ui-id-22" aria-labelledby="ui-id-22" aria-disabled="false" style="width: 100%;"><span class="ui-icon ui-icon-triangle-1-s"></span><span class="ui-selectmenu-text">Monat wählen...</span></span>
                        </div>

                        <div id="searchOrganizer" class="searchBarItem">
                            <select id="agenda_organizer" name="organizer" style="display: none;"><option value="0" selected="selected">Organisator wählen...</option>
                                <option value="9">ENERGIE APÉRO LUZERN</option>
                                <option value="8">ENERGIE APÉRO SCHWYZ</option>
                                <option value="4">Energieberatungszentrale der Zentralschweizer Kantone</option>
                                <option value="6">Energiestadt / EnergieSchweiz für Gemeinden, Zentralschweiz</option>
                                <option value="13">Hochschule Luzern  Technik &amp; Architektur</option>
                                <option value="15">IG Energie-NW</option>
                            </select><span class="ui-selectmenu-button ui-widget ui-state-default ui-corner-all" tabindex="0" id="agenda_organizer-button" role="combobox" aria-expanded="false" aria-autocomplete="list" aria-owns="agenda_organizer-menu" aria-haspopup="true" aria-activedescendant="ui-id-33" aria-labelledby="ui-id-33" aria-disabled="false" style="width: 100%;"><span class="ui-icon ui-icon-triangle-1-s"></span><span class="ui-selectmenu-text">Organisator wählen...</span></span>
                        </div>

                        <div id="clearFilters" style="display: none;">
                            <a href="veranstaltungen/agenda.html#" id="clearFilterLink">
                                Filter löschen
                            </a>
                        </div>

                    </div>

                    <div id="infoBar">
                        <div id="countOfDisplayedEntries">

                            Sichtbar: 1 - 10 von 45

                        </div>
                        <div id="ajaxLoader" style="display: none;">
                            <img alt="ajaxLoader" src="https://www.energie-zentralschweiz.ch/typo3conf/ext/nezzoagenda/Resources/Public/Icons/ajaxLoader.gif" width="16" height="16">
                        </div>
                    </div>

                    <a href="veranstaltungen/agenda.html#" class="showAll">
                        &gt; alle anzeigen
                    </a>
                    -->
                    <div id="tx_nezzoagenda_list" style="display: block;">
                        <input type="hidden" id="agenda_pageUid" autocomplete="off" value="351">
                        <input type="hidden" id="agenda_contentElementId" autocomplete="off" value="1600">

                        <input type="hidden" id="agenda_countOfEntriesStartup" autocomplete="off" value="10">
                        <input type="hidden" id="agenda_countOfAllEvents" autocomplete="off" value="45">

                        <input type="hidden" id="agenda_archiveMode" autocomplete="off" value="">

                        <input type="hidden" id="agenda_currentDisplayedEvents" autocomplete="off" value="10">
                        <input type="hidden" id="agenda_currentSortField" autocomplete="off" value="start_of_event">
                        <input type="hidden" id="agenda_currentSortDirection" autocomplete="off" value="asc">

                        <input type="hidden" id="typeNumPage" autocomplete="off" value="">
                        <input type="hidden" id="currentLanguage" autocomplete="off" value="">

                        <input type="hidden" id="detailsPid" autocomplete="off" value="203">



                        <div class="listThumbnailMonthName">
                            Laufende Workshops
                        </div>


                        <!-- CONTENT START -->
                        <?php foreach($records as $key=>$value) { ?>

                            <?php
                                $eventDate = date("d-m-Y", strtotime($value['date_begin']));
                                $timestamp = strtotime($value['date_begin']);

                                $eventTickets = $events->GetEventTickets($value['event_ticket_ids']);

                                $address = $events->GetResPartnerById($value['address_id'][0]);
                                $organizer = $events->GetResPartnerById($value['organizer_id'][0]);
                                $country = $events->GetResCountryById($address['country_id'][0]);
                            ?>

                            <?php
                            // Abfrage ob laufender Workshop oder nicht
                            if($value['gio_running_workshops'] == 1 && $value['gio_opencourse'] == 0) {
                                ?>

                                <div class="eventItem even">
                                    <div class="dateOpen" style="display: block;">
                                        <span class="glyphicon glyphicon-repeat dateOngoing" aria-hidden="true"></span>
                                    </div>

                                    <div class="dateOpen">
                                        <span class="glyphicon glyphicon-repeat dateOngoing" aria-hidden="true"></span>
                                    </div>

                                    <div id="eventheader-<?php echo $key; ?>" class="eventHeader">
                                        <div class="titleRow">
                                            <div class="titleInformation">
                                                <div class="titleText toggle-<?php echo $key; ?>">
                                                    <?php echo utf8_encode($value['name']); ?>
                                                </div>
                                                <div class="locationDateText toggle-<?php echo $key; ?>">
                                                    Auf Wunsch, Laufende Workshops
                                                </div>
                                                <div class="categoriesText toggle-<?php echo $key; ?>">
                                                    <?php
                                                    $eventType = $events->GetEventType($value['event_type_id'][0]);
                                                    echo '<strong>'.$eventType['name'].'</strong>';
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="eventdetail-<?php echo $key; ?>" class="eventDetails">

                                        <div class="detailsRow">

                                            <div id="detailText" class="detailInformationRow">
                                                <!-- DETAILS -->
                                                <?php echo utf8_encode($value['description']); ?>
                                            </div>

                                            <div id="organizer" class="detailInformationRow">
    <span class="detailsLabel">
    Veranstalter:
    </span>
                                                <span class="detailsValue">
        <div class="name">
            <?php
            echo $organizer['name'].'<br/>'.$organizer['street'].'<br/>'.$organizer['zip'].' '.$organizer['city'].'<br/>'.$country['name'];
            echo '<a href="mailto:'.$organizer['email'].'">'.$organizer['email'].'</a>';
            ?>
        </div>
    </span>
                                            </div>

                                            <div id="organizer" class="detailInformationRow">
    <span class="detailsLabel">
    Veranstaltungsort:
    </span>
                                                <span class="detailsValue">
        <div class="name">
            <?php
            echo $address['name'].'<br/>'.$address['street'].'<br/>'.$address['zip'].' '.$address['city'].'<br/>'.$country['name'];
            echo '<a href="mailto:'.$address['email'].'">'.$address['email'].'</a>';
            ?>
        </div>
    </span>
                                            </div>

                                            <div id="additionalField9" class="detailInformationRow">
				<span class="detailsLabel">
					Kosten:
				</span>
                                                <span class="detailsValue">

                                                    <table class="contenttable contenttable-0 ohneReihen100">
                                                        <!--
                                                        <thead>
                                                        <tr class="tr-even">
                                                            <th>Beschreibung</th>
                                                            <th>Preis</th>
                                                        </tr>
                                                        </thead>
                                                        -->
                                                        <tbody>
                                                            <?php foreach ($eventTickets as $ticket) {?>
                                                                <tr class="tr-odd">
                                                                <td><?php echo utf8_encode($ticket['name']); ?></td>
                                                                <td>CHF <?php echo $ticket['price']; ?>.- inkl. MWST</td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
				</span>
                                            </div>
                                        </div>
                                        <div class="actionRow">
                                            <div class="actionLinks">
                                                <span class="registrationLink">

								<a href="<?php echo 'http://oekowatt01.nine.ch:8069'.$value['website_url']; ?>" target="_blank">
									Anmelden
								</a>

					</span>
                                            </div>
                                        </div>
                                    </div>


                                    <script type="text/javascript">
                                        jQuery(document).ready(function(){

                                            jQuery('.toggle-<?php echo $key; ?>').click(function(){
                                                jQuery('#eventdetail-<?php echo $key; ?>').toggle();
                                            });
                                        });
                                    </script>

                                </div>

                                <!--
            <div class="col-md-4">

                <h4>
                    <?php echo $value['name']; ?>
                </h4>

                <p>
                    <i><?php echo date($value['date_begin']).' - '.date($value['date_end']); ?></i>
                </p>

                <p>
                    <?php
                                $eventType = $events->GetEventType($value['event_type_id'][0]);
                                echo '<strong>'.$eventType['name'].'</strong>';
                                ?>
                </p>

                <p>
                    <?php
                                echo "Favoriten Kurs: ".$value['gio_favcourse']."<br/>Offener Kurs: ".$value['gio_opencourse']
                                ?>
                </p>

                <p>
                    <?php echo $value['description'] ?>
                </p>

                <p>
                    <?php
                                $address = $events->GetResPartnerById($value['address_id'][0]);
                                $country = $events->GetResCountryById($address['country_id'][0]);

                                echo $address['name'].'<br/>'.$address['street'].'<br/>'.$address['zip'].' '.$address['city'].'<br/>'.$country['name'];
                                ?>
                </p>

            </div>
            -->
                            <?php } ?>

                        <?php } ?>
                        <!-- CONTENT ENDE -->

                        <!-- CONTENT START -->
                        <?php
                            $month = array();
                            $monthKey = 0;
                            $monthShow = true;
                        ?>

                        <?php foreach($records as $key=>$value) { ?>

                            <?php
                            $eventDate = date("d.m.Y", strtotime($value['date_begin']));

                            $fromDate = date("d.m.Y, H:i", strtotime($value['date_begin']));
                            $endDate = date("d.m.Y, H:i", strtotime($value['date_end']));

                            $timestamp = strtotime($value['date_begin']);
                            $timestampEnd = strtotime($value['date_end']);

                            $address = $events->GetResPartnerById($value['address_id'][0]);
                            $organizer = $events->GetResPartnerById($value['organizer_id'][0]);
                            $country = $events->GetResCountryById($address['country_id'][0]);

                            $eventTicketId = $value['event_ticket_ids'];

                            $eventTickets = $events->GetEventTickets($value['event_ticket_ids']);

                            /* Monats Angaben Start */
                            $monthDate = date("m Y", strtotime($value['date_begin']));
                            $monthName = $monatsnamenLang[intval($monthDate)];
                            $monthYear = date("Y", strtotime($value['date_begin']));
                            $month[] = $monthDate;

                            if($month[$monthKey] !== $monthDate){
                                //echo $month[$monthKey].' : '.$monthDate.' : '.$monthKey.' : '.$monthShow.'<br/>';
                                $month[] = $monthDate;
                                $monthKey = $monthKey + 1;
                                $monthShow = true;
                                //echo $month[$monthKey].' : '.$monthDate.' : '.$monthKey.' : '.$monthShow;
                            }
                            /* Monats Angaben Start */

                            ?>

                            <?php if($monthShow == true){ ?>
                            <div class="listThumbnailMonthName">
                                <?php
                                    echo $monthName.' '.$monthYear;
                                    $monthShow = false;
                                ?>
                            </div>
                            <?php } ?>

                            <?php
                            // Abfrage ob laufender Workshop oder nicht
                            if($value['gio_running_workshops'] == 0 && $value['gio_opencourse'] == 0) {
                                ?>

                                <div class="eventItem even">

                                    <div class="dateClosed">

                                        <div class="dateDayText">
                                            <?php echo $weekdayName[date('w', $timestamp)]; ?>
                                        </div>
                                        <div class="dateDayNumber">
                                            <?php echo date('d', $timestamp); ?>
                                        </div>
                                        <div class="dateMonthText">
                                            <?php echo $monatsnamen[intval(date('m', $timestamp))]; ?>
                                        </div>

                                        <div class="moreIcon">
                                            <img alt="More" src="https://www.energie-zentralschweiz.ch/fileadmin/template/images/nezzoagenda/dateGraphicMore.png" width="26" height="6">
                                        </div>

                                    </div>

                                    <div class="dateOpen">
                                        <span class="glyphicon glyphicon-repeat dateOngoing" aria-hidden="true"></span>
                                    </div>

                                    <div id="eventheader-<?php echo $key; ?>" class="eventHeader">
                                        <div class="titleRow">
                                            <div class="titleInformation">
                                                <div class="titleText toggle-<?php echo $key; ?>">
                                                    <?php echo utf8_encode($value['name']); ?>
                                                </div>
                                                <div class="locationDateText toggle-<?php echo $key; ?>">
                                                    <?php echo $address['city']; ?>,
                                                    <?php echo $weekdayName[date('w', $timestamp)].', '.$fromDate.' Uhr - '.$weekdayName[date('w', $timestampEnd)].', '.$endDate.' Uhr' ?>
                                                </div>
                                                <div class="categoriesText toggle-<?php echo $key; ?>">
                                                    <?php
                                                    $eventType = $events->GetEventType($value['event_type_id'][0]);
                                                    echo '<strong>'.$eventType['name'].'</strong>';
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="eventdetail-<?php echo $key; ?>" class="eventDetails">

                                        <div class="detailsRow">

                                            <div id="detailText" class="detailInformationRow">
                                                <!-- DETAILS -->
                                                <?php echo utf8_encode($value['description']); ?>
                                            </div>

<div id="organizer" class="detailInformationRow">
    <span class="detailsLabel">
    Veranstalter:
    </span>
    <span class="detailsValue">
        <div class="name">
            <?php
                echo $organizer['name'].'<br/>'.$organizer['street'].'<br/>'.$organizer['zip'].' '.$organizer['city'].'<br/>'.$country['name'];
                echo '<a href="mailto:'.$organizer['email'].'">'.$organizer['email'].'</a>';
            ?>
        </div>
    </span>
</div>

<div id="organizer" class="detailInformationRow">
    <span class="detailsLabel">
    Veranstaltungsort:
    </span>
    <span class="detailsValue">
        <div class="name">
            <?php
            echo $address['name'].'<br/>'.$address['street'].'<br/>'.$address['zip'].' '.$address['city'].'<br/>'.$country['name'];
            echo '<a href="mailto:'.$address['email'].'">'.$address['email'].'</a>';
            ?>
        </div>
    </span>
</div>
                                            <div id="additionalField9" class="detailInformationRow">
				<span class="detailsLabel">
					Kosten:
				</span>
                                                <span class="detailsValue">

                                                    <table class="contenttable contenttable-0 ohneReihen100">
                                                        <!--
                                                        <thead>
                                                        <tr class="tr-even">
                                                            <th>Beschreibung</th>
                                                            <th>Preis</th>
                                                        </tr>
                                                        </thead>
                                                        -->
                                                        <tbody>
                                                            <?php foreach ($eventTickets as $ticket) {?>
                                                            <tr class="tr-odd">
                                                                <td><?php echo utf8_encode($ticket['name']); ?></td>
                                                                <td>CHF <?php echo $ticket['price']; ?>.- inkl. MWST</td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
				</span>
                                            </div>
                                        </div>
                                        <div class="actionRow">
                                            <div class="actionLinks">
	<span class="registrationLink">

								<a href="<?php echo 'http://oekowatt01.nine.ch:8069'.$value['website_url']; ?>" target="_blank">
									Anmelden
								</a>

					</span>
                                            </div>
                                        </div>
                                    </div>


                                    <script type="text/javascript">
                                        jQuery(document).ready(function(){

                                            jQuery('.toggle-<?php echo $key; ?>').click(function(){
                                                jQuery('#eventdetail-<?php echo $key; ?>').toggle();
                                            });
                                        });
                                    </script>

                                </div>

                                <!--
            <div class="col-md-4">

                <h4>
                    <?php echo $value['name']; ?>
                </h4>

                <p>
                    <i><?php echo date($value['date_begin']).' - '.date($value['date_end']); ?></i>
                </p>

                <p>
                    <?php
                                $eventType = $events->GetEventType($value['event_type_id'][0]);
                                echo '<strong>'.$eventType['name'].'</strong>';
                                ?>
                </p>

                <p>
                    <?php
                                echo "Favoriten Kurs: ".$value['gio_favcourse']."<br/>Offener Kurs: ".$value['gio_opencourse']
                                ?>
                </p>

                <p>
                    <?php echo $value['description'] ?>
                </p>

                <p>
                    <?php
                                $address = $events->GetResPartnerById($value['address_id'][0]);
                                $country = $events->GetResCountryById($address['country_id'][0]);

                                echo $address['name'].'<br/>'.$address['street'].'<br/>'.$address['zip'].' '.$address['city'].'<br/>'.$country['name'];
                                ?>
                </p>

            </div>
            -->
                            <?php } ?>

                        <?php } ?>
                        <!-- CONTENT ENDE -->



                    </div>


                    <a href="http://oekowatt01.nine.ch:8069/event" class="showAll">
                        &gt; alle anzeigen
                    </a>


                </div>
            </div>

