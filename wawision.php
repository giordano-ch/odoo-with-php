<?php
/**
 * Created by PhpStorm.
 * User: Andreas Altherr
 * Date: 06.02.2017
 * Time: 14:59
 */

function generateHash()
{

    $initKey = 'EXsl4ywgDR5mR3sTGwws9Ikl2ocSKZlvhxoFTv8Au3MaV5UqrEIJORVF5PmFNy';
    $remoteDomain = 'www.dubdidu.ch';
    $date = gmdate('dmY');
    $hash = "";

    for($i = 0; $i <= 200; $i++)
        $hash = sha1($hash . $initKey . $remoteDomain . $date);

    return $hash;
}

?>

<?php echo generateHash(); ?>
