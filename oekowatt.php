<!DOCTYPE html>
<html xml:lang="de" lang="de" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <!--
        Dieser Webauftritt wurde von internezzo ag erstellt.
            Für Feedback oder Fragen besuchen Sie uns unter: www.internezzo.ch

        This website is powered by TYPO3 - inspiring people to share!
        TYPO3 is a free open source Content Management Framework initially created by Kasper Skaarhoj and licensed under GNU/GPL.
        TYPO3 is copyright 1998-2016 of Kasper Skaarhoj. Extensions are copyright of their respective owners.
        Information and contribution at http://typo3.org/
    -->

    <base href="https://www.energie-zentralschweiz.ch/" />


    <meta name="generator" content="TYPO3 CMS" />


    <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/typo3temp/stylesheet_a490dd7cbb.css?1454315783" media="all" />
    <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/typo3conf/ext/nezzoagenda/Resources/Public/CSS/jquery-ui.min.css?1430201578" media="all" />
    <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/typo3conf/ext/nezzoagenda/Resources/Public/CSS/nezzoagenda.css?1434703695" media="all" />
    <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/fileadmin/template/stylesheets/stylesheet.css?1436935279" media="all" />
    <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/fileadmin/template/stylesheets/browsers/chrome.css?1430888225" media="all" />


    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/jquery-1.8.3.min.js?1423223769" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/typo3conf/ext/nezzoagenda/Resources/Public/JS/jquery-ui.min.js?1430201579" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/menu/menuTablet.js?1429091286" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/picturefill.js?1430397616" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/hideOnMobile.js?1430309332" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/tablesorter/jquery.tablesorter.min.js?1427879817" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/tablesorter/tablesorterConfig.js?1427880155" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/scrollToTop.js?1421911446" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/menu/menuMobile.js?1429087707" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/fancybox/jquery.fancybox.pack.js?1423223775" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/fileadmin/template/javascripts/fancybox/fancyboxConfig.js?1423223774" type="text/javascript"></script>
    <script src="https://www.energie-zentralschweiz.ch/typo3temp/javascript_9a38f34785.js?1454315477" type="text/javascript"></script>


    <title>Odoo Test - Energie Zentralschweiz</title>
    <link rel="shortcut icon" href="https://www.energie-zentralschweiz.ch/favicon.ico" type="image/x-icon" />    <noscript>
        <link rel="stylesheet" type="text/css" href="https://www.energie-zentralschweiz.ch/fileadmin/template/res/noscript.css" />
    </noscript>    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'><meta name="publisher" content="internezzo ag, CH-6343 Rotkreuz, www.internezzo.ch" /><meta name="author" content="Energie Zentralschweiz, 6343 Rotkreuz" /><meta name="dcterms.rights" content="© Energie Zentralschweiz, Alle Rechte vorbehalten" /><meta name="dc.language" content="de" />    <meta name="robots" content="index,follow" /><meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height,user-scalable=yes" /><meta http-equiv="X-UA-Compatible" content="IE=edge">

</head>
<body>
<a id="top"></a>
<header id="header">
    <div id="innerHeaderWrap" class="container">
        <div id="logo" class="col-xs-24 col-sm-18">
            <a href="aktuell-kantone/zentralschweiz.html" title="Startseite"><img src="https://www.energie-zentralschweiz.ch/fileadmin/template/images/logo.png" width="300" height="139"   alt="Startseite"  /></a>
        </div>
        <div id="emblems" class="hidden-xs col-sm-6">
            <a href="aktuell-kantone/zentralschweiz.html" title="Startseite"><img src="https://www.energie-zentralschweiz.ch/fileadmin/template/images/emblems.png" width="600" height="278"   alt="Emblems"  /></a>
        </div>
    </div>
    <div id="naviWrap">
        <nav id="menuHorizontal" class="container hidden-xs hidden-print">
            <ul><li class="no sub"><a href="aktuell-kantone/zentralschweiz.html">Aktuell / Kantone</a><ul><li class="no"><a href="aktuell-kantone/zentralschweiz.html">Zentralschweiz</a></li><li class="no"><a href="aktuell-kantone/news-archiv.html">News-Archiv</a></li><li class="no"><a href="aktuell-kantone/luzern.html">Luzern</a></li><li class="no"><a href="aktuell-kantone/uri.html">Uri</a></li><li class="no"><a href="aktuell-kantone/schwyz.html">Schwyz</a></li><li class="no"><a href="aktuell-kantone/obwalden.html">Obwalden</a></li><li class="no"><a href="aktuell-kantone/nidwalden.html">Nidwalden</a></li><li class="no"><a href="aktuell-kantone/zug.html">Zug</a></li></ul></li><li class="no sub"><a href="vollzug/energienachweise.html">Vollzug</a><ul><li class="no"><a href="vollzug/energienachweise.html">Energienachweise</a></li><li class="no"><a href="vollzug/planungshilfen.html">Planungshilfen</a></li><li class="no"><a href="vollzug/luzern.html">Luzern</a></li><li class="no"><a href="vollzug/uri.html">Uri</a></li><li class="no"><a href="vollzug/schwyz.html">Schwyz</a></li><li class="no"><a href="vollzug/obwalden.html">Obwalden</a></li><li class="no"><a href="vollzug/nidwalden.html">Nidwalden</a></li><li class="no"><a href="vollzug/zug.html">Zug</a></li></ul></li><li class="no sub"><a href="foerderprogramme/zentralschweiz.html">Förderprogramme</a><ul><li class="no"><a href="foerderprogramme/zentralschweiz.html">Zentralschweiz</a></li><li class="no"><a href="foerderprogramme/luzern.html">Luzern</a></li><li class="no"><a href="foerderprogramme/uri.html">Uri</a></li><li class="no"><a href="foerderprogramme/schwyz.html">Schwyz</a></li><li class="no"><a href="foerderprogramme/obwalden.html">Obwalden</a></li><li class="no"><a href="foerderprogramme/nidwalden.html">Nidwalden</a></li><li class="no"><a href="foerderprogramme/zug.html">Zug</a></li></ul></li><li class="no sub"><a href="beratungsstellen/zentralschweiz.html">Beratungsstellen</a><ul><li class="no"><a href="beratungsstellen/zentralschweiz.html">Zentralschweiz</a></li><li class="no"><a href="beratungsstellen/luzern.html">Luzern</a></li><li class="no"><a href="beratungsstellen/uri.html">Uri</a></li><li class="no"><a href="beratungsstellen/schwyz.html">Schwyz</a></li><li class="no"><a href="beratungsstellen/obwalden.html">Obwalden</a></li><li class="no"><a href="beratungsstellen/nidwalden.html">Nidwalden</a></li><li class="no"><a href="beratungsstellen/zug.html">Zug</a></li></ul></li><li class="no sub"><a href="fachinformationen/ausstellungsmaterial.html">Fachinformationen</a><ul><li class="no"><a href="fachinformationen/ausstellungsmaterial.html">Ausstellungsmaterial</a></li><li class="no"><a href="fachinformationen/informationsbroschueren.html">Informationsbroschüren</a></li><li class="no"><a href="fachinformationen/fachliteratur.html">Fachliteratur</a></li><li class="no"><a href="fachinformationen/energiestadt.html">Energiestadt</a></li><li class="no"><a href="fachinformationen/solarenergie.html">Solarenergie</a></li><li class="no"><a href="fachinformationen/gebaeudehuelle.html">Gebäudehülle</a></li><li class="no"><a href="fachinformationen/gebaeudetechnik.html">Gebäudetechnik</a></li><li class="no"><a href="fachinformationen/geak.html">GEAK</a></li><li class="no"><a href="fachinformationen/minergie.html">MINERGIE</a></li><li class="no"><a href="fachinformationen/muken.html">MuKEn</a></li></ul></li><li class="act actSub"><a href="veranstaltungen/agenda.html">Veranstaltungen</a><ul><li class="act"><a href="veranstaltungen/agenda.html">Agenda</a></li><li class="no"><a href="veranstaltungen/rueckblicke.html">Rückblicke</a></li></ul></li><li class="spacer"></li></ul>
        </nav>
    </div>
</header>
<div id="pagewidth" class="container">
    <div id="wrapper">
        <!--TYPO3SEARCH_begin-->
        <main id="maincol" class="col-xs-24 col-sm-18">
            <div id="rootline" class="col-xs-24 hidden-print"><a href="aktuell-kantone/zentralschweiz.html">Aktuell / Kantone</a> > <a href="veranstaltungen/agenda.html">Veranstaltungen</a> > <a href="veranstaltungen/agenda.html">Agenda</a></div>
            <div id="c2103" class="csc-default">

<?php echo file_get_contents('http://localhost:8080/odooreader/classcall.php'); ?>

            </div>
        </main>

        <aside id="rightcol" class="col-xs-24 col-sm-6">
            <div id="searchDiv" class="hidden-print">
                <form action="/system/suchresultate.html" method="post" name="searchform">
                    <label for="tx_indexedsearch_sword">Suchbegriff eingeben</label>
                    <input type="text" name="tx_indexedsearch[sword]" id="tx_indexedsearch_sword" class="search" placeholder="suchen" />
                    <button type="submit" class="submit"><span class="glyphicon glyphicon-search"></span></button>
                    <input type="hidden" name="tx_indexedsearch[_sections]" value="0" />
                    <input type="hidden" name="tx_indexedsearch[pointer]" value="0" />
                    <input type="hidden" name="tx_indexedsearch[ext]" value="0" />
                    <input type="hidden" name="tx_indexedsearch[lang]" value="0" />
                    <input type="hidden" name="tx_indexedsearch[submit_button]" value="Suche" />
                </form>
            </div>

            <?php
                include 'odooreader/aside.php';
            ?>


        </aside>
        <!--TYPO3SEARCH_end-->
    </div>

    <div id="toTopLink" class="glyphicon glyphicon-circle-arrow-up hidden-print"><a href="/index.php?id=373#"><span class="hidden">nach oben</span></a></div>
</div>
<footer id="footer" class="hidden-print">
    <div id="firstRow" class="container">
        <div id="address" class="hidden-xs col-sm-10">
            <div id="c112" class="csc-default"><p>Konferenz Kantonaler Energiefachstellen<br />Regionalkonferenz Zentralschweiz<br />c/o OekoWatt GmbH, Jules Pikali<br />Poststrasse 1<br />6343 Rotkreuz
                </p>
                <p>041 768 66 66<br /><a href="javascript:linkTo_UnCryptMailto('ocknvq,kphqBqgmqycvv0ej');" class="mail">info@<span style="display:none;">STOP-SPAM.</span>oekowatt.ch</a></p></div>
        </div>
        <div id="newsletter" class="col-xs-24 col-sm-8">
            <header><h6>Für den Newsletter anmelden:</h6></header>

            <script>
                function loadjQuery(e,t){var n=document.createElement("script");n.setAttribute("src",e);n.onload=t;n.onreadystatechange=function(){if(this.readyState=="complete"||this.readyState=="loaded")t()};document.getElementsByTagName("head")[0].appendChild(n)}function main(){
                    var $cr=jQuery.noConflict();var old_src;$cr(document).ready(function(){$cr(".cr_form").submit(function(){$cr(this).find('.clever_form_error').removeClass('clever_form_error');$cr(this).find('.clever_form_note').remove();$cr(this).find(".musthave").find('input, textarea').each(function(){if(jQuery.trim($cr(this).val())==""||($cr(this).is(':checkbox'))||($cr(this).is(':radio'))){if($cr(this).is(':checkbox')||($cr(this).is(':radio'))){if(!$cr(this).parent().find(":checked").is(":checked")){$cr(this).parent().addClass('clever_form_error')}}else{$cr(this).addClass('clever_form_error')}}});if($cr(this).attr("action").search(document.domain)>0&&$cr(".cr_form").attr("action").search("wcs")>0){var cr_email=$cr(this).find('input[name=email]');var unsub=false;if($cr("input['name=cr_subunsubscribe'][value='false']").length){if($cr("input['name=cr_subunsubscribe'][value='false']").is(":checked")){unsub=true}}if(cr_email.val()&&!unsub){$cr.ajax({type:"GET",url:$cr(".cr_form").attr("action").replace("wcs","check_email")+$cr(this).find('input[name=email]').val(),success:function(data){if(data){cr_email.addClass('clever_form_error').before("<div class='clever_form_note cr_font'>"+data+"</div>");return false}},async:false})}var cr_captcha=$cr(this).find('input[name=captcha]');if(cr_captcha.val()){$cr.ajax({type:"GET",url:$cr(".cr_form").attr("action").replace("wcs","check_captcha")+$cr(this).find('input[name=captcha]').val(),success:function(data){if(data){cr_captcha.addClass('clever_form_error').after("<div  class='clever_form_note cr_font'>"+data+"</div>");return false}},async:false})}}if($cr(this).find('.clever_form_error').length){return false}return true});$cr('input[class*="cr_number"]').change(function(){if(isNaN($cr(this).val())){$cr(this).val(1)}if($cr(this).attr("min")){if(($cr(this).val()*1)<($cr(this).attr("min")*1)){$cr(this).val($cr(this).attr("min"))}}if($cr(this).attr("max")){if(($cr(this).val()*1)>($cr(this).attr("max")*1)){$cr(this).val($cr(this).attr("max"))}}});old_src=$cr("div[rel='captcha'] img:not(.captcha2_reload)").attr("src");if($cr("div[rel='captcha'] img:not(.captcha2_reload)").length!=0){captcha_reload()}});function captcha_reload(){var timestamp=new Date().getTime();$cr("div[rel='captcha'] img:not(.captcha2_reload)").attr("src","");$cr("div[rel='captcha'] img:not(.captcha2_reload)").attr("src",old_src+"?t="+timestamp);return false}
                }
                if(typeof jQuery==="undefined"){loadjQuery("//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js",main)}else{main()}
            </script>

            <form class="layout_form cr_form cr_font" action="http://86090.seu.cleverreach.com/f/86090-96543/wcs/" method="post" target="_blank">
                <div class="cr_body cr_page cr_font formbox">
                    <div class='non_sortable'>
                    </div>
                    <div class='editable_content'>
                        <div id="2154228" rel="text" class="cr_ipe_item ui-sortable musthave" >
                            <label for="text2154228" class="itemname">Vorname*</label>
                            <input id="text2154228" name="208281" type="text" value="" placeholder="Vorname" />
                        </div>
                        <div id="2154229" rel="text" class="cr_ipe_item ui-sortable musthave" >
                            <label for="text2154229" class="itemname">Nachname*</label>
                            <input id="text2154229" name="208282" type="text" value="" placeholder="Nachname" />
                        </div>
                        <div id="2154224" rel="email" class="cr_ipe_item ui-sortable musthave" >
                            <label for="text2154224" class="itemname">E-Mail*</label>
                            <input id="text2154224" name="email" value="" type="text" placeholder="E-Mail" />
                        </div>
                        <div id="2154226" rel="button" class="cr_ipe_item ui-sortable submit_container" >
                            <button type="submit" class="cr_button">Anmeldung absenden</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <nav id="menuFooter" class="hidden-xs col-sm-6">
            <ul>
                <li class="no"><a href="footer/disclaimer.html">Disclaimer</a></li><li class="no"><a href="footer/impressum.html">Impressum</a></li><li class="no"><a href="footer/sitemap.html">Sitemap</a></li>
                <li id="printlink"><a href="nc/print/veranstaltungen/agenda/odoo-test.html" onclick="vHWin=window.open('https:\/\/www.energie-zentralschweiz.ch\/nc\/print\/veranstaltungen\/agenda\/odoo-test.html','FEopenLink','toolbar=no,status=no,scrollbars=yes,menubar=no,location=no,titlebar=no,resizable=no,width=850,height=720');vHWin.focus();return false;">Druckversion</a></li>
            </ul>
        </nav>
    </div>
</footer>

<div id="menuMobileWrap"><div id="navigatorDivHeader" class="hidden-print visible-xs"><span class="glyphicon glyphicon-menu-hamburger"></span><span id="menuText">Menü</span></div><div id="navigatorDivAddressHeader" class="hidden-print visible-xs"><span id="menuInfoText">Info</span><span class="glyphicon glyphicon-info-sign"></span></div></div>
<div id="menuMobile" class="visible-xs hidden-print">
    <div id="navigatorDiv"><ul><li class="sub"><span class="toggleNavi glyphicon glyphicon-chevron-right"></span><a href="aktuell-kantone/zentralschweiz.html" onclick="blur();">Aktuell / Kantone</a><ul><li class="no"><a href="aktuell-kantone/zentralschweiz.html" onclick="blur();">Zentralschweiz</a></li><li class="no"><a href="aktuell-kantone/news-archiv.html" onclick="blur();">News-Archiv</a></li><li class="no"><a href="aktuell-kantone/luzern.html" onclick="blur();">Luzern</a></li><li class="no"><a href="aktuell-kantone/uri.html" onclick="blur();">Uri</a></li><li class="no"><a href="aktuell-kantone/schwyz.html" onclick="blur();">Schwyz</a></li><li class="no"><a href="aktuell-kantone/obwalden.html" onclick="blur();">Obwalden</a></li><li class="no"><a href="aktuell-kantone/nidwalden.html" onclick="blur();">Nidwalden</a></li><li class="no"><a href="aktuell-kantone/zug.html" onclick="blur();">Zug</a></li></ul></li><li class="sub"><span class="toggleNavi glyphicon glyphicon-chevron-right"></span><a href="vollzug/energienachweise.html" onclick="blur();">Vollzug</a><ul><li class="no"><a href="vollzug/energienachweise.html" onclick="blur();">Energienachweise</a></li><li class="no"><a href="vollzug/planungshilfen.html" onclick="blur();">Planungshilfen</a></li><li class="no"><a href="vollzug/luzern.html" onclick="blur();">Luzern</a></li><li class="no"><a href="vollzug/uri.html" onclick="blur();">Uri</a></li><li class="no"><a href="vollzug/schwyz.html" onclick="blur();">Schwyz</a></li><li class="no"><a href="vollzug/obwalden.html" onclick="blur();">Obwalden</a></li><li class="no"><a href="vollzug/nidwalden.html" onclick="blur();">Nidwalden</a></li><li class="no"><a href="vollzug/zug.html" onclick="blur();">Zug</a></li></ul></li><li class="sub"><span class="toggleNavi glyphicon glyphicon-chevron-right"></span><a href="foerderprogramme/zentralschweiz.html" onclick="blur();">Förderprogramme</a><ul><li class="no"><a href="foerderprogramme/zentralschweiz.html" onclick="blur();">Zentralschweiz</a></li><li class="no"><a href="foerderprogramme/luzern.html" onclick="blur();">Luzern</a></li><li class="no"><a href="foerderprogramme/uri.html" onclick="blur();">Uri</a></li><li class="no"><a href="foerderprogramme/schwyz.html" onclick="blur();">Schwyz</a></li><li class="no"><a href="foerderprogramme/obwalden.html" onclick="blur();">Obwalden</a></li><li class="no"><a href="foerderprogramme/nidwalden.html" onclick="blur();">Nidwalden</a></li><li class="no"><a href="foerderprogramme/zug.html" onclick="blur();">Zug</a></li></ul></li><li class="sub"><span class="toggleNavi glyphicon glyphicon-chevron-right"></span><a href="beratungsstellen/zentralschweiz.html" onclick="blur();">Beratungsstellen</a><ul><li class="no"><a href="beratungsstellen/zentralschweiz.html" onclick="blur();">Zentralschweiz</a></li><li class="no"><a href="beratungsstellen/luzern.html" onclick="blur();">Luzern</a></li><li class="no"><a href="beratungsstellen/uri.html" onclick="blur();">Uri</a></li><li class="no"><a href="beratungsstellen/schwyz.html" onclick="blur();">Schwyz</a></li><li class="no"><a href="beratungsstellen/obwalden.html" onclick="blur();">Obwalden</a></li><li class="no"><a href="beratungsstellen/nidwalden.html" onclick="blur();">Nidwalden</a></li><li class="no"><a href="beratungsstellen/zug.html" onclick="blur();">Zug</a></li></ul></li><li class="sub"><span class="toggleNavi glyphicon glyphicon-chevron-right"></span><a href="fachinformationen/ausstellungsmaterial.html" onclick="blur();">Fachinformationen</a><ul><li class="no"><a href="fachinformationen/ausstellungsmaterial.html" onclick="blur();">Ausstellungsmaterial</a></li><li class="no"><a href="fachinformationen/informationsbroschueren.html" onclick="blur();">Informationsbroschüren</a></li><li class="no"><a href="fachinformationen/fachliteratur.html" onclick="blur();">Fachliteratur</a></li><li class="no"><a href="fachinformationen/energiestadt.html" onclick="blur();">Energiestadt</a></li><li class="no"><a href="fachinformationen/solarenergie.html" onclick="blur();">Solarenergie</a></li><li class="no"><a href="fachinformationen/gebaeudehuelle.html" onclick="blur();">Gebäudehülle</a></li><li class="no"><a href="fachinformationen/gebaeudetechnik.html" onclick="blur();">Gebäudetechnik</a></li><li class="no"><a href="fachinformationen/geak.html" onclick="blur();">GEAK</a></li><li class="no"><a href="fachinformationen/minergie.html" onclick="blur();">MINERGIE</a></li><li class="no"><a href="fachinformationen/muken.html" onclick="blur();">MuKEn</a></li></ul></li><li class="act actSub toggled"><span class="toggleNavi glyphicon glyphicon-chevron-down"></span><a href="veranstaltungen/agenda.html" onclick="blur();">Veranstaltungen</a><ul><li class="cur"><a href="veranstaltungen/agenda.html" onclick="blur();">Agenda</a></li><li class="no"><a href="veranstaltungen/rueckblicke.html" onclick="blur();">Rückblicke</a></li></ul></li></ul><ul><li class="no"><a href="footer/disclaimer.html" onclick="blur();">Disclaimer</a></li><li class="no"><a href="footer/impressum.html" onclick="blur();">Impressum</a></li><li class="no"><a href="footer/sitemap.html" onclick="blur();">Sitemap</a></li></ul></div>
</div>

<div id="menuMobileInfo" class="visible-xs hidden-print">
    <div id="c678" class="csc-default"><p>Konferenz Kantonaler Energiefachstellen
        </p>
        <p>Regionalkonferenz Zentralschweiz
        </p>
        <p>c/o OekoWatt, Jules Pikali
        </p>
        <p>Poststrasse 1
        </p>
        <p>6343 Rotkreuz</p></div>
</div>




</body>
</html>