<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP & Odoo</title>

    <link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>PHP & Odoo</h1>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">
            <ul>
                <li><a href="info.php">PHP Info</a></li>
                <li><a href="odooreader/classcall.php">Odoo Events (mit PHP Klassen)</a></li>
                <li><a href="odooreader/simplecall.php">Odoo Events (innerhalb des HTML Codes)</a></li>
            </ul>
        </div>

    </div>

</div>


<script src="node_modules/jquery.2/node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>